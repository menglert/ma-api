import http.client
import json
import getpass
import keyring
import logging

logging.basicConfig(level=logging.DEBUG, filename='ma-api.log')

try:
    configFile = open("config.json")
    logging.info("Reading configuration from: " + configFile.name)
    
    configJson = json.load(configFile)
    apiKey = configJson["apiKey"]
    zsCloud = configJson["zsCloud"]
    secretKey = keyring.get_password("ma-api-secret", apiKey)
    if apiKey is None or secretKey is None:
        raise IOError("No Credential set.")
    logging.info("Read API Secret for API Key ID: " + apiKey + " for Cloud: " + zsCloud)
except IOError:
    logging.info("No config present. Creating one.")
    configJson = {}
    apiKey = input("API Key ID: ")
    secretKey = getpass.getpass(prompt="API Secret: ")
    zsCloud = input("Zscaler Cloud: ")
    configJson["apiKey"] = apiKey
    configJson["zsCloud"] = zsCloud
    configFile = open("config.json", "w")
    json.dump(configJson, configFile)
    logging.info("Set API Secret for API Key ID: " + apiKey + " for Cloud: " + zsCloud + " in " + configFile.name)

    configFile.close()
    keyring.set_password("ma-api-secret", apiKey, secretKey)
    logging.info("Set API Secret for API Key ID: " + apiKey + " for Cloud: " + zsCloud + " in Keyring")
finally:
    configFile.close()

conn = http.client.HTTPSConnection("api-mobile." + zsCloud)

payload = {"apiKey": apiKey ,"secretKey": secretKey}

headers = {
    'content-type': "application/json",
}
logging.debug("Using Headers: " + str(headers))

conn.request("POST", "/papi/auth/v1/login", json.dumps(payload), headers)
res = conn.getresponse()
data = res.read().decode("utf-8")
logging.debug("Received Response: " + data)

jwtToken = json.loads(data)["jwtToken"]

headers = { 
    'content-type': 'application/json',
    'auth-token': jwtToken
}

logging.debug("Using Headers: " + str(headers))
logging.info("Logged In.")

