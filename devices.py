import logging
import json

logging.basicConfig(level=logging.DEBUG, filename='ma-api.log')

def getDownloadDevicess(headers, conn, osTypes="", registrationTypes=""):
    conn.request("GET", "/papi/public/v1/downloadDevices?osTypes=" + osTypes + "&registrationTypes=" + registrationTypes, headers=headers)

    res = conn.getresponse()
    data = res.read().decode("utf-8")

    logging.debug("GET downloadDevices Response: " + str(data))

    return data

def getDevices(headers, conn, osType="", page=1, pageSize=30):
    conn.request("GET", "/papi/public/v1/getDevices?osType=" + osType + "&page=" + str(page) + "&pageSize=" + str(pageSize), headers=headers)

    res = conn.getresponse()
    data = res.read().decode("utf-8")

    logging.debug("GET getDevices Response: " + str(data))

    return json.loads(data)

def removeDevices(headers, conn, payload, pageSize=30):
    logging.debug("Using payload for POST removeDevices: " + json.dumps(payload))

    conn.request("POST", "/papi/public/v1/removeDevices?pageSize=" + str(pageSize), json.dumps(payload), headers=headers)

    res = conn.getresponse()
    data = res.read().decode("utf-8")

    logging.debug("POST removeDevices Response: " + str(data))

    return json.loads(data)

def forceRemoveDevices(headers, conn, payload, pageSize=30):
    logging.debug("Using payload for POST forceRemoveDevices: " + json.dumps(payload))

    conn.request("POST", "/papi/public/v1/forceRemoveDevices?pageSize=" + str(pageSize), json.dumps(payload), headers=headers)

    res = conn.getresponse()
    data = res.read().decode("utf-8")

    logging.debug("POST forceRemoveDevices Response: " + str(data))

    return json.loads(data)
