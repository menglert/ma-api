import logging
import json

logging.basicConfig(level=logging.DEBUG, filename='ma-api.log')

def getOtp(headers, conn, udid):
    conn.request("GET", "/papi/public/v1/getOtp?udid=" + udid, headers=headers)

    res = conn.getresponse()
    data = res.read().decode("utf-8")

    logging.debug("GET getOtp Response: " + str(data))

    return json.loads(data)

def getPasswords(headers, conn, username,osType):
    conn.request("GET", "/papi/public/v1/getPasswords?osType=" + str(osType) + "&username=" + username, headers=headers)

    res = conn.getresponse()
    data = res.read().decode("utf-8")

    logging.debug("GET getPasswords Response: " + str(data))

    return json.loads(data)
