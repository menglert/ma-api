import logging
from devices import getDownloadDevicess
from devices import getDevices
from devices import removeDevices
from devices import forceRemoveDevices
from passwords import getOtp
from passwords import getPasswords
from authenticateSession import conn
from authenticateSession import headers

logging.basicConfig(level=logging.DEBUG, filename='ma-api.log')

#print(getDownloadDevicess(headers, conn))
#print(getDevices(headers, conn))
#print(getOtp(headers, conn,"Test-UDID"))
#print(getPasswords(headers, conn, "test@test.com", 3))
payload = {
  "clientConnectorVersion": [
    "string"
  ],
  "osType": 0,
  "udids": [
    "string"
  ],
  "userName": "string"
}
#print(removeDevices(headers, conn, payload))
#print(forceRemoveDevices(headers, conn, payload))